//
//  LogoCollectionViewCell.swift
//  MixTeam
//
//  Created by Renaud JENNY on 09/09/2017.
//  Copyright © 2017 Renaud JENNY. All rights reserved.
//

import UIKit

class LogoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var logoImageView: UIImageView!
}
