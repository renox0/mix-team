//
//  PlayerTableViewCell.swift
//  MixTeam
//
//  Created by Renaud JENNY on 01/09/2017.
//  Copyright © 2017 Renaud JENNY. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
