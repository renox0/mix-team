//
//  TeamHeaderView.swift
//  MixTeam
//
//  Created by Renaud JENNY on 20/08/2017.
//  Copyright © 2017 Renaud JENNY. All rights reserved.
//

import UIKit

class TeamHeaderView: UITableViewHeaderFooterView {
    @IBOutlet var view: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var logoImageView: UIImageView!
}
